const walleta = document.querySelector("#btn_pay_walleta");
const ghesta = document.querySelector("#btn_pay_ghesta");



walleta.addEventListener("click", function () {
    const btn = document.querySelector("#btn_pay_walleta")
    const modal = document.querySelector("#form_modal_walleta")
    const form = document.querySelector("#pay_form_walleta")
    const close = document.querySelector('#close')
    const submit = document.querySelector('.sub_text')
    const toggleModal = function () {
        if (modal.classList.contains("active")) {
            modal.classList.remove("active")
        } else {
            modal.classList.add("active")
        }
    }
    const handleSubmitForm = function (ev) {
        submit.innerHTML = ''
        submit.classList.add('loader')
        // console.log(submit)
        ev.preventDefault();

        const inputs = form.querySelectorAll("input")
        var formdata = new FormData();

        for (let input of inputs) {
          formdata.append(input.name, input.value);

        }
        console.log(formdata)
        const slug = form.querySelector("#slug").value
        const request_url = `/api/pardakht/w/${slug}/walleta/`
        fetch( request_url, {
            method: "POST",
            redirect: 'follow',
            body: formdata
        })
            .then((res) => {
              // window.location.href = request_url;
              return res.text()
            }).then((text) =>{
              var parser = new DOMParser();
	            var doc = parser.parseFromString(text, 'text/html');
	            var redirect_url = doc.querySelector('#bank-form');
	            if (redirect_url) {
                modal.classList.remove("active")
                submit.classList.remove('loader')
                submit.innerHTML = 'ارسال'
                window.location.href = redirect_url.action
              }
              else {
                var error = doc.querySelector('.ui.fluid');
                window.alert(error.textContent)
                modal.classList.remove("active")
                submit.classList.remove('loader')
                submit.innerHTML = 'ارسال'
              }
              // console.log(text)
            })
            // .then(json => {
            //     // after success "form" posted
            //     console.log(json)
            // })
            .catch((err) => console.error(err))

    }

    form.addEventListener("submit", handleSubmitForm)
    btn.addEventListener("click", toggleModal)
    close.addEventListener("click", toggleModal)

})

ghesta.addEventListener("click", function () {
    const btn = document.querySelector("#btn_pay_ghesta")
    const modal = document.querySelector("#form_modal_ghesta")
    const form = document.querySelector("#pay_form_ghesta")
    const close = document.querySelector('#close')
    const submit = document.querySelector('.sub_text')
    const toggleModal = function () {
        if (modal.classList.contains("active")) {
            modal.classList.remove("active")
        } else {
            modal.classList.add("active")
        }
    }
    const handleSubmitForm = function (ev) {
        submit.innerHTML = ''
        submit.classList.add('loader')
        // console.log(submit)
        ev.preventDefault();

        const inputs = form.querySelectorAll("input")
        var formdata = new FormData();

        for (let input of inputs) {
          formdata.append(input.name, input.value);

        }
        console.log(formdata)
        const slug = form.querySelector("#slug").value
        const request_url = `/api/pardakht/w/${slug}/ghesta/`
        fetch( request_url, {
            method: "POST",
            redirect: 'follow',
            body: formdata
        })
            .then((res) => {
              // window.location.href = request_url;
              return res.text()
            }).then((text) =>{
              var parser = new DOMParser();
	            var doc = parser.parseFromString(text, 'text/html');
	            var redirect_url = doc.querySelector('#bank-form');
	            if (redirect_url) {
                modal.classList.remove("active")
                submit.classList.remove('loader')
                submit.innerHTML = 'ارسال'
                window.location.href = redirect_url.action
              }
              else {
                var error = doc.querySelector('.ui.fluid');
                window.alert(error.textContent)
                modal.classList.remove("active")
                submit.classList.remove('loader')
                submit.innerHTML = 'ارسال'
              }
              // console.log(text)
            })
            // .then(json => {
            //     // after success "form" posted
            //     console.log(json)
            // })
            .catch((err) => console.error(err))

    }

    form.addEventListener("submit", handleSubmitForm)
    btn.addEventListener("click", toggleModal)
    close.addEventListener("click", toggleModal)

})
