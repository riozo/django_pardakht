import requests
from django.http import HttpRequest
from django.conf import settings
from django.urls import reverse
import logging
import json
from django.utils import timezone
from orders.models import Order

name = 'walleta'
display_name = 'درگاه والتا'
get_token_url = "https://cpg.walleta.ir/payment/request.json"
verification_url = "https://cpg.walleta.ir/payment/verify.json"

logger = logging.getLogger(__name__)


def redirect_url(payment):
    return "https://cpg.walleta.ir/ticket/{}".format(payment.token)

def get_orderitems(payment):
    order = Order.objects.get(payment=payment)
    items = order.get_items()
    items_data = []
    for item in items:
        p,e = item.get_product()
        items_data.append({
                            "name": p.name,
                            "quantity": item.count,
                            "unit_price" : item.get_discounted_unit_price(),
                            "unit_discount" : 0,
                            "unit_tax_amount" : 0,
                            "total_amount" : item.estimated_price()
                            })
    return items_data

def redirect_data(request:HttpRequest, payment):
    return {}


def send_request(url, data):
    headers = {'Content-type': 'application/json'}
    req  = requests.post(url, data=json.dumps(data), headers=headers)
    if req.status_code != 200:
        return False, req.json()
    else:
        return req.json(), False

def get_token(request: HttpRequest, payment, data):
    merchant_id = getattr(settings, str(name+'_merchant_id').upper(), 'none')
    if merchant_id == 'none':
        logger.error('Merchant ID not in settings.\nDefine your merchant id in settings.py as '+str(name+'_merchant_id').upper())
        return None

    data = {
            "invoice_reference" : payment.trace_number,
            "invoice_date" : str(timezone.now()),
            "payer_first_name" : data.get('payer_first_name', None),
            "payer_last_name" : data.get('payer_last_name', None),
            "payer_national_code" : data.get('payer_national_code', None),
            "payer_mobile" : data.get('payer_mobile', None),
            "items" : get_orderitems(payment),
            "merchant_code" : merchant_id,
            "invoice_amount" : payment.price,
            "description": payment.description,
            "callback_url" : request.build_absolute_uri(reverse('pardakht:callback_url', args=[payment.slug, name])).replace('http://', 'https://')
    }
    result, error = send_request(get_token_url, data)
    if result:
        payment.gateway = name
        payment.save()
        return result['token'], False
    else:
        logger.error("Couldn't get payment token from walleta")
        return False, error


def verify(request, payment):
    merchant_id = getattr(settings, str(name + '_merchant_id').upper(), 'none')
    if merchant_id == 'none':
        logger.error('Merchant ID not in settings.\nDefine your merchant id in settings.py as ' + str(
            name + '_merchant_id').upper())
        return None
    # get payment info from vandar and if its okay confirm the transaction
    result, error = send_request(  verification_url,
                            data={  "merchant_code":merchant_id,
                                    "token":payment.token,
                                    "invoice_reference":payment.trace_number,
                                    "invoice_amount":payment.price}
                                    )
    if result and result['is_paid']:
        payment.state = payment.STATE_SUCCESS
    else:
        payment.state = payment.STATE_FAILURE
    payment.save()
