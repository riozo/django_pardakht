import requests
from django.http import HttpRequest
from django.conf import settings
from django.urls import reverse
import logging
import json
from django.utils import timezone
from orders.models import Order

name = 'ghesta'
display_name ='درگاه قسطا'
get_token_url = "‫‪https://api.payping.ir/v2/pay‬‬"
verification_url = "‫‪https://api.payping.ir/v2/pay/verify‬‬"

logger = logging.getLogger(__name__)


def redirect_url(payment):
    return "‫‪http://payping.ir/installment/{}?type=ghesta‬‬".format(payment.token)



def redirect_data(request:HttpRequest, payment):
    return {}


def send_request(url, data):
    merchant_id = getattr(settings, str(name+'_merchant_id').upper(), 'none')
    if merchant_id == 'none':
        logger.error('Merchant ID not in settings.\nDefine your merchant id in settings.py as '+str(name+'_merchant_id').upper())
        return None
    headers = {'Content-type': 'application/json',
                '‫‪Authorization':‬‬ '‫‪bearer‬‬ {}‬‬'.format(merchant_id)
                }
    req  = requests.post(url, data=json.dumps(data), headers=headers)
    if req.status_code != 200:
        return False, req.json()
    else:
        return req.json(), False

def get_token(request: HttpRequest, payment, data):
    merchant_id = getattr(settings, str(name+'_merchant_id').upper(), 'none')
    if merchant_id == 'none':
        logger.error('Merchant ID not in settings.\nDefine your merchant id in settings.py as '+str(name+'_merchant_id').upper())
        return None
    data = {
            "‫‪payerName‬‬" : data.get('‫‪payerName‬‬', None),
            "‫‪payerIndentity‬‬" : data.get('‫‪payerIndentity‬‬', None),
            "amount" : payment.price,
            "‫‪returnURL‬‬" : request.build_absolute_uri(reverse('pardakht:callback_url', args=[payment.slug, name])).replace('http://', 'https://'),
            "description": payment.description,
            "‫‪clientRefId‬‬" : payment.trace_number,
    }
    result, error = send_request(get_token_url, data)
    if result:
        payment.gateway = name
        payment.save()
        return result['code'], False
    else:
        logger.error("Couldn't get payment token from walleta")
        return False, error


def verify(request, payment):
    merchant_id = getattr(settings, str(name + '_merchant_id').upper(), 'none')
    if merchant_id == 'none':
        logger.error('Merchant ID not in settings.\nDefine your merchant id in settings.py as ' + str(
            name + '_merchant_id').upper())
        return None
    # get payment info from vandar and if its okay confirm the transaction
    refrence_id = request.data
    result, error = send_request(  verification_url,
                            data={
                                    "amount":payment.price,
                                    "refid":refrence_id,
                                    }
                                    )
    if result and result.get("‫‪cardHashPan‬‬", None):
        payment.state = payment.STATE_SUCCESS
    else:
        payment.state = payment.STATE_FAILURE
    payment.save()
