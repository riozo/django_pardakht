import requests
from django.http import HttpRequest
from django.conf import settings
from django.urls import reverse
import logging

name = 'vandar2'
display_name = 'بانکی وندار ۲'
get_token_url = "https://ipg.vandar.io/api/v3/send"
verification_url = "https://ipg.vandar.io/api/v3/verify"

logger = logging.getLogger(__name__)


def redirect_url(payment):
    return "https://ipg.vandar.io/v3/{}".format(payment.token)


def redirect_data(request:HttpRequest, payment):
    return {}


def send_request(url, data):
    req  = requests.post(url, data=data)
    return req.json()


def get_token(request: HttpRequest, payment):
    merchant_id = getattr(settings, str(name+'_merchant_id').upper(), 'none')
    if merchant_id == 'none':
        logger.error('Merchant ID not in settings.\nDefine your merchant id in settings.py as '+str(name+'_merchant_id').upper())
        return None
    data = {
            "api_key" : merchant_id,
            "amount" : payment.price * 10,
            "description": payment.description,
            "callback_url" : request.build_absolute_uri(reverse('pardakht:callback_url', args=[payment.slug, name])).replace('http://', 'https://')
    }
    result = send_request(get_token_url, data)
    if result['status'] == 1:
        payment.gateway = name
        payment.save()
        return result['token']
    else:
        logger.error("Couldn't get payment token from zarinpal")
        return None


def verify(request, payment):
    if request.GET.get('payment_status') != 'OK':
        payment.state = payment.STATE_FAILURE
        payment.payment_result = str(request.GET.get('payment_status'))
        payment.save()
        return

    merchant_id = getattr(settings, str(name + '_merchant_id').upper(), 'none')
    if merchant_id == 'none':
        logger.error('Merchant ID not in settings.\nDefine your merchant id in settings.py as ' + str(
            name + '_merchant_id').upper())
        return None
    # get payment info from vandar and if its okay confirm the transaction
    result = send_request(verification_url, data={"api_key":merchant_id,"token":payment.token})
    if result['status'] == 1:
        payment.state = payment.STATE_SUCCESS
        payment.ref_number = result['transId']
    else:
        payment.state = payment.STATE_FAILURE
    payment.verification_result = str(result['message'])
    payment.save()
